import json
from botocore.vendored import requests

def lambda_handler(event, context):
    response = requests.get("http://ipvigilante.com/" + event["queryStringParameters"]["ip"])
    print("Response body: %s" % response.text)
    location_dict = response.json()
    # TODO implement
    location = {
        "latitude": location_dict["data"]["latitude"],
        "longitude": location_dict["data"]["longitude"]
    }
    return {
        'statusCode': 200,
        'body': json.dumps(location)
    }
