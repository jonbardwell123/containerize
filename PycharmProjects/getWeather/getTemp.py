def get_temperature(longitude, latitude):
    api_url = ' https://api.darksky.net/forecast/{key}/ {lat},{long} '
    url = api_url.format(key=DARK_SKY_SECRET_KEY, lat=latitude,
        long=longitude)
    data = requests.get(url)
    forecast = data.json()
return forecast['currently']['temperature']