import json
from botocore.vendored import requests

DARK_SKY_SECRET_KEY="fbcf15d3d872f2f66720690d60e26343"

def lambda_handler(event, context):
    latitude = event["queryStringParameters"]["latitude"]
    longitude = event["queryStringParameters"]["longitude"]
    api_url = f"https://api.darksky.net/forecast/{DARK_SKY_SECRET_KEY}/{latitude},{longitude}"

    data = requests.get(api_url)
    response_dict = data.json()
    print(response_dict)
    forecast = {
        "summary": response_dict.get("currently").get("summary"),
        "temperature": response_dict.get("currently").get("temperature"),
        "humidity": response_dict.get("currently").get("humidity"),
        "apparentTemperature": response_dict.get("currently").get("apparentTemperature"),
        "high": response_dict.get("daily").get("data")[0].get("temperatureHigh"),
        "low": response_dict.get("daily").get("data")[0].get("temperatureLow")
    }
    return {
        "body": forecast,
        "status_code": 200
    }